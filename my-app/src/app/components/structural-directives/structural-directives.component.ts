import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrls: ['./structural-directives.component.css']
})
export class StructuralDirectivesComponent implements OnInit {

  users: User[] =[];

  constructor() { }

  ngOnInit(): void {

    this.users = [
      {
        firstName: "Clark",
        lastName: "Kent",
        age: 35
      },
      {
        firstName: "Bruce",
        lastName: "Wayne",
        age: 37
      },
      {
        firstName: "Tony",
        lastName: "Stark",
        age: 32
      }
    ]

  }

}
