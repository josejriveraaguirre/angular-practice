import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StructuralDirectivesComponent } from './components/structural-directives/structural-directives.component';
import { StringInterpolationComponent } from './components/string-interpolation/string-interpolation.component';

@NgModule({
  declarations: [
    AppComponent,
    StructuralDirectivesComponent,
    StringInterpolationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
